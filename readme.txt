=== WP App Enabler ===
Contributors: mrpaplu
Tags: webapp
Requires at least: 3.0.1
Tested up to: 3.8.1
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enables your WordPress site to run in App mode in Android.

== Description ==

Enables your WordPress site to run in App mode in Android.

== Installation ==

1. Upload the `wp-app-enabler`-folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

or:

1. Install the plugin via the WordPress repository
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= Do I need to do anything after installation? =

No, you can now add your WordPress site to your homescreen on Android.

== Changelog ==

= 0.1 =
* First release.

== Upgrade Notice ==

= 0.1 =
This is the first released version of the plugin. There's probably not going to be many more, because it's super basic.