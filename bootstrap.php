<?php
/*
Plugin Name: WP App Enabler
Plugin URI: http://tumblersedge.nl/
Description: Enables your WordPress site to run in App mode in Android.
Version: 0.1
Author: Kay van Bree
Author URI: http://tumblersedge.nl/
*/

if(!function_exists('enable_android_app_mode')){
    add_action('wp_head', 'enable_android_app_mode');
    add_action('admin_head', 'enable_android_app_mode');
    
    function enable_android_app_mode(){
        ?><meta name="mobile-web-app-capable" content="yes"><?php
    }
}
?>
